/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Util;

/**
 *
 * @author docenteauditorio
 */
public class Dato<T> {

    private T info;

    public Dato() {
    }

    public Dato(T info) {
        this.info = info;
    }

    public T getInfo() {
        return info;
    }

    public void setInfo(T info) {
        this.info = info;
    }

    @Override
    public String toString() {
        return "Dato{" + "info=" + info + '}';
    }

    public void imprimirPorConsola() {
        System.out.println(this.info.toString());
    }

    public T getMenor(T objeto2) {
        int c = ((Comparable) (info)).compareTo(objeto2);
//        if (c <= 0) {
//            return this.info;
//        }
        if (c > 0) {
            return objeto2;
        }
       return this.info;
    }

    
    public T getMenor2(Dato<T> objeto2) {
        int c = ((Comparable) (info)).compareTo(objeto2.info);
//        if (c <= 0) {
//            return this.info;
//        }
        if (c > 0) {
            return objeto2.info;
        }
       return this.info;
    }
}
